import static org.mockito.Mockito.*;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.*;

public class Tests {
    class Calculation {
        Calculator calculator;
        public Calculation(Calculator calculator) {
            this.calculator = calculator;
        }
        public void calculate() {
            calculator.add(2,5);
        }
    }

    @Mock
    Calculator calculatorMock; 

    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule(); 

    @Test
    public void testQuery()  {
        Calculation t  = new Calculation(calculatorMock); 
        t.calculate();
        verify(calculatorMock).add(2,5); 
    }

    @Test
    public void test1()  {
        //  create mock
        Calculator test = mock(Calculator.class);

        // define return value for method getUniqueId()
        when(test.add(2l,5l)).thenReturn(7l);

        // use mock in test....
        assertEquals(test.add(2,5), 7);
    }
}